provider "aws" {
  region = "${var.region}"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_access_key}"
}

resource "aws_instance" "jenkins_server" {
  ami = "ami-055d15d9cfddf7bd3"
  instance_type = "${var.instance_type}"
  availability_zone = "${var.avalibility_zones}"
  
  network_interface {
    device_index = 0
    network_interface_id = "${aws_network_interface.web_server_jk.id}"
  }

#   user_data = filebase64("run_jk.sh")

  tags = {
    Name = "Jenkins_server"
  }
}
