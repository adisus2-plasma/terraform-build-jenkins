#create vpc and subnet
resource "aws_vpc" "jenkins_vpc" {
  cidr_block = "20.0.0.0/16"

  tags = {
      Name = "jenkins_vpc"
  }
}

resource "aws_subnet" "jenkins_subnet" {
  vpc_id = aws_vpc.jenkins_vpc.id
  cidr_block = "20.0.2.0/24"
  availability_zone = "${var.avalibility_zones}"

  tags = {
    Name = "jenkins_subnet"
  }
}

#create gateway and rtb
resource "aws_internet_gateway" "jenkins_gw" {
  vpc_id = aws_vpc.jenkins_vpc.id

  tags = {
    Name = "jenkins_gw"
  }
}

resource "aws_route_table" "jenkins_rtb" {
  vpc_id = aws_vpc.jenkins_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.jenkins_gw.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.jenkins_gw.id
  }

  tags = {
    Name = "jenkins_rtb"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id = aws_subnet.jenkins_subnet.id
  route_table_id = aws_route_table.jenkins_rtb.id
}

#create sec-group
resource "aws_security_group" "jenkins_sg" {
  name = "jenkins_sg"
  vpc_id = aws_vpc.jenkins_vpc.id

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 447
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "jenkins_sg"
  }
}

#create nw_interface
resource "aws_network_interface" "web_server_jk" {
  subnet_id = aws_subnet.jenkins_subnet.id
  private_ips = ["20.0.2.40"]
  security_groups = [aws_security_group.jenkins_sg.id]

  tags = {
    Name = "jk_web_server"
  }
}

#create eip
resource "aws_eip" "jk_pub_ip" {
  vpc = true
  network_interface = aws_network_interface.web_server_jk.id
  associate_with_private_ip = "20.0.2.40"
  depends_on = [
    aws_internet_gateway.jenkins_gw
  ]

  tags = {
    Name = "jk_eip"
  }
}

